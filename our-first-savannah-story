<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta property="og:url" content="https://sunnysavannahs.com/"><meta property="og:type" content="website"><meta property="og:title" content="Sunny Savannahs"><meta property="og:description" content="Savannah cat breeder in Cincinnati, Ohio. Specializing in high-quality F2 and F3 Savannahs."><meta property="og:image" content="https://lh3.googleusercontent.com/9rnDDSirxnHjbra-x2UT4x4LP4YnK3U6vOIXN9-xgPjlKwA14A=w1200-no"><meta name="google-site-verification" content="rpjUZszyWbz3F14O_TS9DtrvWtQaIaOkKgKisrXzsjs"><title>Our First Savannah Story - Sunny Savannahs</title><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="/stylesheets/style.css"><link rel="icon" type="image/png" href="/favicon.png"></head><body><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script><div id="fb-root"></div><script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&version=2.5";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script><nav class="navbar navbar-default navbar-static-top"><div class="container"><div class="navbar-header"><button type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/"><img src="https://lh3.googleusercontent.com/SRxhQ1NvjguknsZNJ6Bl_HoNJVuZ_jZsJ9L5WbaGDXe3A6NLKw=w350-no" style="width: 250px; margin-top: 9px; margin-left: 5px;" alt="Sunny Savannahs Logo"></a></div><div id="navbar" class="collapse navbar-collapse"><ul class="nav navbar-right nav-pills"><li class="inactive"><a href="/">Home</a></li><li class="active"><a href="/our-first-savannah-story">Our First Savannah Story</a></li><li class="inactive"><a href="/our-cattery">Our Cattery</a></li><li class="inactive"><a href="/available-kittens">Available Kittens</a></li><li class="inactive"><a href="/about-us">About Us</a></li><li class="inactive"><a href="/savannah-fun">Savannah Fun</a></li><li class="inactive"><a href="/adoption-process">About Adoptions</a></li></ul></div></div></nav><div id="body-content" class="container"><h1>Our First Savannah Story</h1><div class="row">
<div class="col-md-8">
<p>Sunny was our first Savannah. He is a gorgeous, high-percentage F2, black-spotted, golden boy that got us to love Savannahs. When Sunny first came home he had us a little worried. We thought: &quot;this isn&#39;t a regular cat, he has wild in him, are we going to regret this?&quot; He bounced around for hours on end playing with everything in sight...shoelaces, a wire whisk, a paintbrush...he wasn&#39;t that discerning. Everything was magical to him, except us; we were sort of boring to him. He also played rough at times, not realizing that his sharp kitten teeth were not enjoyable to our fingers.
</p>
<p>With a little coaching and patience we learned that he would bond with us. After about a month he began showing interest in us and giving head-butts. He also began to bite much softer once we learned to let out whimpering sounds as feedback that his play bite was too hard. He&#39;d look at us concerned and then gently lick the area as if to say &quot;I&#39;m sorry, I just get too excited.&quot; By six months he was following us around and sleeping at the foot of our bed. By one year, he was our eager companion greeting us when we got home with an upright, wiggly tail.
</p>
<p>Now he shares the shower with us, chasing the shower spray. Several times he&#39;s hopped in my warm bath before I&#39;ve gotten to it and even while I&#39;m in it! His life-long passion is water, as it is for most Savannahs. The older he gets the better companion he has become and it is clear he loves us dearly. He licks our faces regularly in an affectionate way and tries to groom our hair with little success. Higher generation Savannahs tend to bond with one or two people, and Sunny is bonded with us. When distressed at the vet he will burry his head in my stomach and give me a head-butt in the chest, and then hiss at the vet as if to say &quot;I know you’re going to do things I don’t like, so I don&#39;t like you!&quot; Earning his love has been most endearing. Besides the vet he is polite to everyone else. We are happy to name our cattery after him. We find him to be quite the character.</p>
<p>Our relationships with each of our savannahs is different because each savannah has their unique personality, but never have we regretted owning any of our cats. They are members of our family that we love on every day.</p>
</div>
<div class="col-md-4">
<div class="thumbnail">
<img src="https://lh3.googleusercontent.com/peoa-rGSyltYrBXNMbDguulOeseeiCkUuMlXM7QjU0b2GcQWxg=w600-no" alt="Sitting Sunny" style="width: 100%;">
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="thumbnail">
<img src="https://lh3.googleusercontent.com/lFItZnA-cczlQHDZnvkkjs8QZsm0DFET_oHafqwwloo9FCOP4ZBP=w700-no" alt="Sunny in the bathtub"/>
</div>
<div class="thumbnail">
<img src="https://lh3.googleusercontent.com/yu1lvw8TJA9-e2MRklfpZ5ke8fIv9IGIY-txKfawSOeKhLc_-OaY=w700-no" alt="Young Sunny sleeping"/>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
<img src="https://lh3.googleusercontent.com/uR_y8aFD-kR_7sbqf_BB3ciLVeZzEDl1UOFFuiqIFYAwCyukBerX=w700-no" alt="Meg and Sunny"/>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
<img src="https://lh3.googleusercontent.com/jx0JrMEJd0f0HwnkoeVL-ie3A8Xo0zNWg4ET5fumKnEAN4oK-A=w600-no" alt="Sunny kissing Dan"/>
</div>
</div>
</div></div><footer class="footer"><div class="container"><div class="row"><div class="col-md-11 col-sm-12"><div style="padding: 15px 0 10px 0;"><span class="text-muted">Contact Us:</span><ol class="contact-list"><li><a href="tel:440-630-0270">440-630-0270</a></li><li><a href="/cdn-cgi/l/email-protection#7201071c1c0b011304131c1c131a0132151f131b1e5c111d1f"><span class="__cf_email__" data-cfemail="2a795f444453794b5c4b44444b42596a4d474b434604494547">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></li></ol></div></div><div class="col-md-1 col-sm-12"><div style="padding: 15px 0 10px 0;"><Connect></Connect><div data-href="https://facebook.com/sunnysavannahs" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false" class="fb-like"></div></div></div></div></div><div style="padding: 0 15px 10px 15px;" class="container"><ul><li><span class="text-muted">Savannah cat breeder in Cincinnati, Ohio</span></li></ul></div><div style="padding: 0 15px 15px 15px;" class="container"><ul><li><span class="text-muted">&copy; 2015 Sunny Savannahs</span></li></ul></div></footer><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script><script src="/js/salvattore.min.js"></script><script src="/js/site.js"></script><script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-68038509-1', 'auto');
ga('send', 'pageview');</script><script type="text/javascript">/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body></html>